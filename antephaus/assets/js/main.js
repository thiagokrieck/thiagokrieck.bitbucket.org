 $(document).ready(function(){
   $(window).bind('scroll', function() {
   var navHeight = $(window).height() - 115;
     if ($(window).scrollTop() > navHeight) {
       $('.nav-bar').addClass('fixed');
       $('.fullmenu').addClass('visible');
     }
     else {
       $('.nav-bar').removeClass('fixed');
       $('.fullmenu').removeClass('visible');
     }
  });

  //Bug correction on mobile. On android browsers the background imagem "jumps" with scrolling as the url bar disapears.
  var bg = jQuery(".hero-img");
  	jQuery(window).resize("resizeBackground");
  	function resizeBackground() {
  	    bg.height(jQuery(window).height()-110);
  }
  resizeBackground();

  $( ".tabs" ).tabs();
  
  //MOBILE MENU
  $(".menu-icon").on("tap",function() {
    $("nav").fadeToggle();
    $(".fixed").addClass("mobile-menu-opened");
  });
  $("nav a").on("tap",function() {
    $("nav").fadeToggle();
    $(".fixed").removeClass("mobile-menu-opened");
  });
  //WOCHENKARTE MOBILE MENU
  
  $(".wochenkarte-mob-menu").on("tap",function() {
    $(".tabs-overlay").fadeToggle();
  });
  
  $(".ui-tabs-nav a").on("touchstart",function() {
    $(".tabs-overlay").fadeToggle();
    var menuName = $(this).text()
    $(".wochenkarte-mob-menu span").text(menuName);
     
  });

});
   //Maps
  var map;

 function initMap() {
  var myLatLng = {lat: 48.79363, lng: 9.19248};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    center: myLatLng
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: 'Antephaus'
  });
}
